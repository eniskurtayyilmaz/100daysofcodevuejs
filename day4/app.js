window.vue = new Vue({
    el: "#app",
    name: "Cart",
    data: {
        isLoading: true,
        cart: [],
        saved: []
    },
    created() {
        fetch("http://localhost:3000/cart")
            .then((res) => {
                return res.json();
            })
            .then((res) => {
                console.log(res);
                this.isLoading = false;
                this.cart = res;
            });

        fetch("http://localhost:3000/saved")
            .then((res) => {
                return res.json();
            })
            .then((res) => {
                console.log(res);
                this.isLoading = false;
                this.saved = res;
            });
    },
    methods: {
        removeFromCart(index) {
            //Array'deki index'e git ve 1 adet kayıt çıkar
            this.cart.splice(index, 1);
        },
        saveForLater(index) {
            const item = this.cart.splice(index, 1);
            this.saved.push(item[0]);
        },
        removeFromSavedList(index) {
            //Array'deki index'e git tüm kayıtları çıkar
            this.saved.splice(index);
        },
        moveToCart(index) {
            const item = this.saved.splice(index, 1);
            this.cart.push(item[0]);
        }
    },
    computed: {
        cartTotal() {
            let total = 0;
            this.cart.forEach(element => {
                total += parseFloat(element.price, 10);
            });
            return total.toFixed(2);
        }
    }
});