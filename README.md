# 100daysofcodevuejs

Try to understand vue.js next 100 days.

# Day 1

- What's Vue? Concept etc.
- Vue (pronounced /vjuː/, like view)

# Day 2

- What is the _el_
- Define Vue.js like below

```js
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
```

- v-if conditions
- v-show vs v-if
- v-html vs v-text

# Day 3

    First commit includes:
        - Understand created function
        - using fetch function

    Second commit includes:
        - Understand loops
        - Define and how to use methods in app.js
        - v-bind:href usages

# Day 4

    - What is Computed Property?
    - How to use computed property in App.js

# Day 5

    First commit includes:
        - How to create Component in Vue.js
        - Define props, method, computed and template keys in Component
        - Template must be contain only one root html.
        - When components calls in a js must be careful camelcase, uppercase. Such as, Vue.component('VueCart', props); You should call <vue-cart></vue-cart> in your html.
