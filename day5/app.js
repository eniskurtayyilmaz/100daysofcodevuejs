const props = {
    props: {
        cart: {
            type: Array,
            required: true
        },
        title: {
            type: String,
            required: true
        }
    },
    computed: {
        cartTotal() {
            let total = 0;
            this.cart.forEach(element => {
                total += parseFloat(element.price, 10);
            });
            return total.toFixed(2);
        }
    },
    template: `
    <div class="card-wrapper">
        <h2>{{title}}</h2>
        <div class="card">
            <div class="item" v-for="(item, index) in cart">
                <div class="image">
                    <a v-bind:href="item.url">
                    <img v-bind:src="item.image" />
                    </a>
                </div>
                <div class="info">
                    <h4>{{item.name}}</h4>
                    <p class="seller">by {{item.seller}}</p>
                    <p class="status available" v-if="item.isAvailable">In Stock</p>
                    <p class="shipping" v-if="item.isEligible">Eligible for FREE Shipping & FREE Returns</p>
                    <a href="#" v-on:click="removeFromCart(index)">Delete</a>
                    <a href="#" class="secondary" v-on:click="saveForLater(index)">Save for later</a>
                </div>
                <p class="price">\${{item.price}}</p>
            </div>
        </div>
        <div class="subtotal">
            Subtotal ({{cart.length}} items) : <span class="price">\${{cartTotal}}</span>
        </div>

    </div>
 `,
    methods: {
        removeFromCart(index) {
            //Array'deki index'e git ve 1 adet kayıt çıkar
            this.cart.splice(index, 1);
        }
    }
};
Vue.component('VueCart', props);


window.vue = new Vue({
    el: "#app",
    name: "Cart",
    data: {
        isLoading: true,
        cart: [],
        saved: []
    },
    created() {
        fetch("http://localhost:3000/cart")
            .then((res) => {
                return res.json();
            })
            .then((res) => {
                console.log(res);
                this.isLoading = false;
                this.cart = res;
            });

        fetch("http://localhost:3000/saved")
            .then((res) => {
                return res.json();
            })
            .then((res) => {
                console.log(res);
                this.isLoading = false;
                this.saved = res;
            });
    },
    methods: {
        removeFromCart(index) {
            //Array'deki index'e git ve 1 adet kayıt çıkar
            this.cart.splice(index, 1);
        },
        saveForLater(index) {
            const item = this.cart.splice(index, 1);
            this.saved.push(item[0]);
        },
        removeFromSavedList(index) {
            //Array'deki index'e git tüm kayıtları çıkar
            this.saved.splice(index);
        },
        moveToCart(index) {
            const item = this.saved.splice(index, 1);
            this.cart.push(item[0]);
        }
    }
});